
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use ieee.std_logic_arith.ALL;

entity ROM_AB is
	port(
		CLK : in STD_LOGIC;
		A : in std_logic_vector(3 downto 0);
		D : out STD_LOGIC_VECTOR(19 downto 0)
		);
end ROM_AB;

architecture ROM_AB of ROM_AB is
begin
	process (A)
		variable A_temp:integer;
	begin	   
		A_temp:= conv_integer(A(3 downto 0));	
				case A_temp is
					when 0 => D <= x"00009";
					when 1 => D <= x"05009";
					when 2 => D <= x"6C008";
					when 3 => D <= x"6E00C";
					when 4 => D <= x"FC000";
					when 5 => D <= x"FF008";
					when 6 => D <= x"F8000";
					when 7 => D <= x"EF00C";
					when 8 => D <= x"EF006";
					when 9 => D <= x"F3006";
					when others => D <= "ZZZZZZZZZZZZZZZZZZZZ";
				end case;			
	end process;
end ROM_AB;
