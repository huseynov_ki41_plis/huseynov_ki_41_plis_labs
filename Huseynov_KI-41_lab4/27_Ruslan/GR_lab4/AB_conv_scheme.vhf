--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : AB_conv_scheme.vhf
-- /___/   /\     Timestamp : 12/05/2016 10:30:38
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -sympath C:/Users/Dima/Desktop/AB_lab4/ipcore_dir -intstyle ise -family virtex7 -flat -suppress -vhdl C:/Users/Dima/Desktop/AB_lab4/AB_conv_scheme.vhf -w C:/Users/Dima/Desktop/AB_lab4/AB_conv_scheme.sch
--Design Name: AB_conv_scheme
--Device: virtex7
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--
----- CELL FD16CE_HXILINX_AB_conv_scheme -----


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FD16CE_HXILINX_AB_conv_scheme is
port (
    Q   : out STD_LOGIC_VECTOR(15 downto 0) := (others => '0');

    C   : in STD_LOGIC;
    CE  : in STD_LOGIC;
    CLR : in STD_LOGIC;
    D   : in STD_LOGIC_VECTOR(15 downto 0)
    );
end FD16CE_HXILINX_AB_conv_scheme;

architecture Behavioral of FD16CE_HXILINX_AB_conv_scheme is

begin

process(C, CLR)
begin
  if (CLR='1') then
    Q <= (others => '0');
  elsif (C'event and C = '1') then
    if (CE='1') then 
      Q <= D;
    end if;
  end if;
end process;


end Behavioral;

----- CELL FD8CE_HXILINX_AB_conv_scheme -----


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FD8CE_HXILINX_AB_conv_scheme is
port (
    Q   : out STD_LOGIC_VECTOR(7 downto 0) := (others => '0');

    C   : in STD_LOGIC;
    CE  : in STD_LOGIC;
    CLR : in STD_LOGIC;
    D   : in STD_LOGIC_VECTOR(7 downto 0)
    );
end FD8CE_HXILINX_AB_conv_scheme;

architecture Behavioral of FD8CE_HXILINX_AB_conv_scheme is

begin

process(C, CLR)
begin
  if (CLR='1') then
    Q <= (others => '0');
  elsif (C'event and C = '1') then
    if (CE='1') then 
      Q <= D;
    end if;
  end if;
end process;


end Behavioral;


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity AB_conv_scheme is
   port ( CE     : in    std_logic; 
          CLK    : in    std_logic; 
          CLR    : in    std_logic; 
          GR_A   : in    std_logic_vector (7 downto 0); 
          GR_B   : in    std_logic_vector (7 downto 0); 
          GR_D   : in    std_logic_vector (7 downto 0); 
          GR_RES : out   std_logic_vector (19 downto 0));
end AB_conv_scheme;

architecture BEHAVIORAL of AB_conv_scheme is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal ADDER1_OUT : std_logic_vector (19 downto 0);
   signal ADDER1_1   : std_logic_vector (19 downto 0);
   signal ADDER1_2   : std_logic_vector (19 downto 0);
   signal ADDER2_OUT : std_logic_vector (19 downto 0);
   signal ADDER2_1   : std_logic_vector (19 downto 0);
   signal ADDER2_2   : std_logic_vector (19 downto 0);
   signal B_stage1   : std_logic_vector (7 downto 0);
   signal MULB_1     : std_logic_vector (7 downto 0);
   signal MUL_BD_OUT : std_logic_vector (15 downto 0);
   signal MUL_D_1    : std_logic_vector (7 downto 0);
   signal MUL1_A     : std_logic_vector (7 downto 0);
   signal MUL1_OUT   : std_logic_vector (15 downto 0);
   signal XLXN_73    : std_logic;
   signal XLXN_74    : std_logic;
   signal XLXN_77    : std_logic;
   signal XLXN_78    : std_logic;
   signal XLXN_79    : std_logic;
   signal XLXN_80    : std_logic;
   signal XLXN_81    : std_logic;
   signal XLXN_82    : std_logic;
   signal XLXN_118   : std_logic;
   signal XLXN_119   : std_logic;
   signal XLXN_137   : std_logic;
   signal XLXN_138   : std_logic;
   signal XLXN_193   : std_logic;
   signal XLXN_207   : std_logic_vector (15 downto 0);
   signal XLXN_209   : std_logic;
   signal XLXN_211   : std_logic;
   signal XLXN_212   : std_logic_vector (7 downto 0);
   signal XLXN_241   : std_logic;
   signal XLXN_242   : std_logic;
   signal XLXN_266   : std_logic;
   signal XLXN_267   : std_logic_vector (15 downto 0);
   signal XLXN_275   : std_logic_vector (15 downto 0);
   signal XLXN_288   : std_logic_vector (7 downto 0);
   signal XLXN_289   : std_logic_vector (7 downto 0);
   signal XLXN_290   : std_logic_vector (7 downto 0);
   signal XLXN_291   : std_logic;
   signal XLXN_293   : std_logic;
   signal XLXN_294   : std_logic;
   component FD8CE_HXILINX_AB_conv_scheme
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic_vector (7 downto 0); 
             Q   : out   std_logic_vector (7 downto 0));
   end component;
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component AB_MUL_8_16
      port ( a   : in    std_logic_vector (7 downto 0); 
             b   : in    std_logic_vector (7 downto 0); 
             clk : in    std_logic; 
             p   : out   std_logic_vector (15 downto 0));
   end component;
   
   component FD16CE_HXILINX_AB_conv_scheme
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic_vector (15 downto 0); 
             Q   : out   std_logic_vector (15 downto 0));
   end component;
   
   component BUS_16_20_CONV
      port ( in_sig  : in    std_logic_vector (15 downto 0); 
             out_sig : out   std_logic_vector (19 downto 0));
   end component;
   
   component AB_ADDER_16_32
      port ( a   : in    std_logic_vector (19 downto 0); 
             b   : in    std_logic_vector (19 downto 0); 
             clk : in    std_logic; 
             ce  : in    std_logic; 
             s   : out   std_logic_vector (19 downto 0));
   end component;
   
   component GR_register20
      port ( ld  : in    std_logic; 
             clr : in    std_logic; 
             clk : in    std_logic; 
             d   : in    std_logic_vector (19 downto 0); 
             q   : out   std_logic_vector (19 downto 0));
   end component;
   
   component BUS_8_16_CONV
      port ( data_in  : in    std_logic_vector (7 downto 0); 
             data_out : out   std_logic_vector (15 downto 0));
   end component;
   
   attribute HU_SET of XLXI_5 : label is "XLXI_5_0";
   attribute HU_SET of XLXI_15 : label is "XLXI_15_1";
   attribute HU_SET of XLXI_18 : label is "XLXI_18_2";
   attribute HU_SET of XLXI_21 : label is "XLXI_21_3";
   attribute HU_SET of XLXI_66 : label is "XLXI_66_7";
   attribute HU_SET of XLXI_79 : label is "XLXI_79_4";
   attribute HU_SET of XLXI_132 : label is "XLXI_132_5";
   attribute HU_SET of XLXI_154 : label is "XLXI_154_6";
   attribute HU_SET of XLXI_161 : label is "XLXI_161_8";
   attribute HU_SET of XLXI_162 : label is "XLXI_162_9";
begin
   XLXI_5 : FD8CE_HXILINX_AB_conv_scheme
      port map (C=>CLK,
                CE=>XLXN_74,
                CLR=>XLXN_73,
                D(7 downto 0)=>GR_A(7 downto 0),
                Q(7 downto 0)=>MUL1_A(7 downto 0));
   
   XLXI_10 : GND
      port map (G=>XLXN_73);
   
   XLXI_11 : VCC
      port map (P=>XLXN_74);
   
   XLXI_15 : FD8CE_HXILINX_AB_conv_scheme
      port map (C=>CLK,
                CE=>XLXN_78,
                CLR=>XLXN_77,
                D(7 downto 0)=>GR_B(7 downto 0),
                Q(7 downto 0)=>B_stage1(7 downto 0));
   
   XLXI_16 : GND
      port map (G=>XLXN_77);
   
   XLXI_17 : VCC
      port map (P=>XLXN_78);
   
   XLXI_18 : FD8CE_HXILINX_AB_conv_scheme
      port map (C=>CLK,
                CE=>XLXN_80,
                CLR=>XLXN_79,
                D(7 downto 0)=>GR_B(7 downto 0),
                Q(7 downto 0)=>MULB_1(7 downto 0));
   
   XLXI_19 : GND
      port map (G=>XLXN_79);
   
   XLXI_20 : VCC
      port map (P=>XLXN_80);
   
   XLXI_21 : FD8CE_HXILINX_AB_conv_scheme
      port map (C=>CLK,
                CE=>XLXN_82,
                CLR=>XLXN_81,
                D(7 downto 0)=>GR_D(7 downto 0),
                Q(7 downto 0)=>MUL_D_1(7 downto 0));
   
   XLXI_22 : GND
      port map (G=>XLXN_81);
   
   XLXI_23 : VCC
      port map (P=>XLXN_82);
   
   XLXI_39 : AB_MUL_8_16
      port map (a(7 downto 0)=>MUL1_A(7 downto 0),
                b(7 downto 0)=>MUL1_A(7 downto 0),
                clk=>CLK,
                p(15 downto 0)=>MUL1_OUT(15 downto 0));
   
   XLXI_58 : GND
      port map (G=>XLXN_118);
   
   XLXI_59 : VCC
      port map (P=>XLXN_119);
   
   XLXI_66 : FD8CE_HXILINX_AB_conv_scheme
      port map (C=>CLK,
                CE=>XLXN_119,
                CLR=>XLXN_118,
                D(7 downto 0)=>B_stage1(7 downto 0),
                Q(7 downto 0)=>XLXN_212(7 downto 0));
   
   XLXI_76 : VCC
      port map (P=>XLXN_138);
   
   XLXI_78 : GND
      port map (G=>XLXN_137);
   
   XLXI_79 : FD16CE_HXILINX_AB_conv_scheme
      port map (C=>CLK,
                CE=>XLXN_138,
                CLR=>XLXN_137,
                D(15 downto 0)=>MUL1_OUT(15 downto 0),
                Q(15 downto 0)=>XLXN_267(15 downto 0));
   
   XLXI_128 : VCC
      port map (P=>XLXN_193);
   
   XLXI_132 : FD8CE_HXILINX_AB_conv_scheme
      port map (C=>CLK,
                CE=>XLXN_211,
                CLR=>XLXN_209,
                D(7 downto 0)=>XLXN_212(7 downto 0),
                Q(7 downto 0)=>XLXN_290(7 downto 0));
   
   XLXI_133 : VCC
      port map (P=>XLXN_211);
   
   XLXI_134 : GND
      port map (G=>XLXN_209);
   
   XLXI_139 : BUS_16_20_CONV
      port map (in_sig(15 downto 0)=>XLXN_207(15 downto 0),
                out_sig(19 downto 0)=>ADDER2_2(19 downto 0));
   
   XLXI_141 : AB_ADDER_16_32
      port map (a(19 downto 0)=>ADDER2_1(19 downto 0),
                b(19 downto 0)=>ADDER2_2(19 downto 0),
                ce=>XLXN_193,
                clk=>CLK,
                s(19 downto 0)=>ADDER2_OUT(19 downto 0));
   
   XLXI_148 : VCC
      port map (P=>XLXN_242);
   
   XLXI_149 : GND
      port map (G=>XLXN_241);
   
   XLXI_151 : GR_register20
      port map (clk=>CLK,
                clr=>CLR,
                d(19 downto 0)=>ADDER2_OUT(19 downto 0),
                ld=>CE,
                q(19 downto 0)=>GR_RES(19 downto 0));
   
   XLXI_152 : GR_register20
      port map (clk=>CLK,
                clr=>CLR,
                d(19 downto 0)=>ADDER1_OUT(19 downto 0),
                ld=>CE,
                q(19 downto 0)=>ADDER2_1(19 downto 0));
   
   XLXI_153 : AB_MUL_8_16
      port map (a(7 downto 0)=>MULB_1(7 downto 0),
                b(7 downto 0)=>MUL_D_1(7 downto 0),
                clk=>CLK,
                p(15 downto 0)=>MUL_BD_OUT(15 downto 0));
   
   XLXI_154 : FD16CE_HXILINX_AB_conv_scheme
      port map (C=>CLK,
                CE=>XLXN_242,
                CLR=>XLXN_241,
                D(15 downto 0)=>MUL_BD_OUT(15 downto 0),
                Q(15 downto 0)=>XLXN_275(15 downto 0));
   
   XLXI_156 : BUS_16_20_CONV
      port map (in_sig(15 downto 0)=>XLXN_275(15 downto 0),
                out_sig(19 downto 0)=>ADDER1_2(19 downto 0));
   
   XLXI_157 : BUS_16_20_CONV
      port map (in_sig(15 downto 0)=>XLXN_267(15 downto 0),
                out_sig(19 downto 0)=>ADDER1_1(19 downto 0));
   
   XLXI_158 : AB_ADDER_16_32
      port map (a(19 downto 0)=>ADDER1_1(19 downto 0),
                b(19 downto 0)=>ADDER1_2(19 downto 0),
                ce=>XLXN_266,
                clk=>CLK,
                s(19 downto 0)=>ADDER1_OUT(19 downto 0));
   
   XLXI_159 : VCC
      port map (P=>XLXN_266);
   
   XLXI_160 : BUS_8_16_CONV
      port map (data_in(7 downto 0)=>XLXN_289(7 downto 0),
                data_out(15 downto 0)=>XLXN_207(15 downto 0));
   
   XLXI_161 : FD8CE_HXILINX_AB_conv_scheme
      port map (C=>CLK,
                CE=>XLXN_294,
                CLR=>XLXN_291,
                D(7 downto 0)=>XLXN_290(7 downto 0),
                Q(7 downto 0)=>XLXN_288(7 downto 0));
   
   XLXI_162 : FD8CE_HXILINX_AB_conv_scheme
      port map (C=>CLK,
                CE=>XLXN_293,
                CLR=>XLXN_291,
                D(7 downto 0)=>XLXN_288(7 downto 0),
                Q(7 downto 0)=>XLXN_289(7 downto 0));
   
   XLXI_163 : VCC
      port map (P=>XLXN_294);
   
   XLXI_164 : VCC
      port map (P=>XLXN_293);
   
   XLXI_165 : GND
      port map (G=>XLXN_291);
   
end BEHAVIORAL;


