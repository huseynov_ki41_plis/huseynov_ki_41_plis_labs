--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : AB_not_conv_scheme.vhf
-- /___/   /\     Timestamp : 12/05/2016 16:10:52
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -sympath C:/Users/Dima/Desktop/AB_lab4/ipcore_dir -intstyle ise -family virtex7 -flat -suppress -vhdl C:/Users/Dima/Desktop/AB_lab4/AB_not_conv_scheme.vhf -w C:/Users/Dima/Desktop/AB_lab4/AB_not_conv_scheme.sch
--Design Name: AB_not_conv_scheme
--Device: virtex7
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity AB_not_conv_scheme is
   port ( CLK    : in    std_logic; 
          GR_A   : in    std_logic_vector (7 downto 0); 
          GR_B   : in    std_logic_vector (7 downto 0); 
          GR_D   : in    std_logic_vector (7 downto 0); 
          ZERO   : in    std_logic_vector (3 downto 0); 
          CU     : out   std_logic_vector (19 downto 0); 
          GR_RES : out   std_logic_vector (19 downto 0));
end AB_not_conv_scheme;

architecture BEHAVIORAL of AB_not_conv_scheme is
   attribute BOX_TYPE   : string ;
   signal ADDER_OUT       : std_logic_vector (19 downto 0);
   signal MUL_OUT         : std_logic_vector (19 downto 0);
   signal MUX_4           : std_logic_vector (19 downto 0);
   signal MUX1_OUT        : std_logic_vector (19 downto 0);
   signal MUX2_OUT        : std_logic_vector (19 downto 0);
   signal RAM1_OUT        : std_logic_vector (19 downto 0);
   signal RAM2_OUTAdderB  : std_logic_vector (19 downto 0);
   signal RAM3_OUT_Addera : std_logic_vector (19 downto 0);
   signal RAM4_OUT        : std_logic_vector (19 downto 0);
   signal XLXN_52         : std_logic_vector (3 downto 0);
   signal XLXN_55         : std_logic;
   signal XLXN_57         : std_logic;
   signal XLXN_59         : std_logic;
   signal XLXN_61         : std_logic;
   signal XLXN_156        : std_logic_vector (19 downto 0);
   signal XLXN_178        : std_logic_vector (19 downto 0);
   signal XLXN_180        : std_logic_vector (19 downto 0);
   signal CU_DUMMY        : std_logic_vector (19 downto 0);
   component ROM_AB
      port ( CLK : in    std_logic; 
             A   : in    std_logic_vector (3 downto 0); 
             D   : out   std_logic_vector (19 downto 0));
   end component;
   
   component AB_Counter
      port ( clk : in    std_logic; 
             q   : out   std_logic_vector (3 downto 0));
   end component;
   
   component AB_RAM
      port ( WE  : in    std_logic; 
             CE  : in    std_logic; 
             OE  : in    std_logic; 
             CLK : in    std_logic; 
             A   : in    std_logic_vector (3 downto 0); 
             DI  : in    std_logic_vector (19 downto 0); 
             DQ  : out   std_logic_vector (19 downto 0));
   end component;
   
   component MUL_20
      port ( a   : in    std_logic_vector (19 downto 0); 
             b   : in    std_logic_vector (19 downto 0); 
             clk : in    std_logic; 
             p   : out   std_logic_vector (19 downto 0));
   end component;
   
   component AB_ADDER_16_32
      port ( a   : in    std_logic_vector (19 downto 0); 
             b   : in    std_logic_vector (19 downto 0); 
             clk : in    std_logic; 
             ce  : in    std_logic; 
             s   : out   std_logic_vector (19 downto 0));
   end component;
   
   component MUX_20
      port ( s0     : in    std_logic; 
             CLK    : in    std_logic; 
             data1  : in    std_logic_vector (19 downto 0); 
             data2  : in    std_logic_vector (19 downto 0); 
             data_o : out   std_logic_vector (19 downto 0));
   end component;
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component BUS_8_20_CONV
      port ( data_in  : in    std_logic_vector (7 downto 0); 
             data_out : out   std_logic_vector (19 downto 0));
   end component;
   
   component GR_register20
      port ( ld  : in    std_logic; 
             clr : in    std_logic; 
             clk : in    std_logic; 
             d   : in    std_logic_vector (19 downto 0); 
             q   : out   std_logic_vector (19 downto 0));
   end component;
   
begin
   CU(19 downto 0) <= CU_DUMMY(19 downto 0);
   XLXI_2 : ROM_AB
      port map (A(3 downto 0)=>XLXN_52(3 downto 0),
                CLK=>CLK,
                D(19 downto 0)=>CU_DUMMY(19 downto 0));
   
   XLXI_9 : AB_Counter
      port map (clk=>CLK,
                q(3 downto 0)=>XLXN_52(3 downto 0));
   
   XLXI_13 : AB_RAM
      port map (A(3 downto 0)=>ZERO(3 downto 0),
                CE=>XLXN_61,
                CLK=>CLK,
                DI(19 downto 0)=>XLXN_156(19 downto 0),
                OE=>CU_DUMMY(15),
                WE=>XLXN_61,
                DQ(19 downto 0)=>RAM1_OUT(19 downto 0));
   
   XLXI_14 : AB_RAM
      port map (A(3 downto 0)=>ZERO(3 downto 0),
                CE=>XLXN_59,
                CLK=>CLK,
                DI(19 downto 0)=>MUX_4(19 downto 0),
                OE=>CU_DUMMY(14),
                WE=>XLXN_59,
                DQ(19 downto 0)=>RAM2_OUTAdderB(19 downto 0));
   
   XLXI_15 : AB_RAM
      port map (A(3 downto 0)=>CU_DUMMY(7 downto 4),
                CE=>XLXN_57,
                CLK=>CLK,
                DI(19 downto 0)=>MUL_OUT(19 downto 0),
                OE=>CU_DUMMY(13),
                WE=>XLXN_57,
                DQ(19 downto 0)=>RAM3_OUT_Addera(19 downto 0));
   
   XLXI_16 : AB_RAM
      port map (A(3 downto 0)=>CU_DUMMY(11 downto 8),
                CE=>XLXN_55,
                CLK=>CLK,
                DI(19 downto 0)=>XLXN_180(19 downto 0),
                OE=>CU_DUMMY(12),
                WE=>XLXN_55,
                DQ(19 downto 0)=>RAM4_OUT(19 downto 0));
   
   XLXI_27 : MUL_20
      port map (a(19 downto 0)=>MUX1_OUT(19 downto 0),
                b(19 downto 0)=>MUX2_OUT(19 downto 0),
                clk=>CLK,
                p(19 downto 0)=>MUL_OUT(19 downto 0));
   
   XLXI_28 : AB_ADDER_16_32
      port map (a(19 downto 0)=>RAM2_OUTAdderB(19 downto 0),
                b(19 downto 0)=>RAM3_OUT_Addera(19 downto 0),
                ce=>CU_DUMMY(2),
                clk=>CLK,
                s(19 downto 0)=>ADDER_OUT(19 downto 0));
   
   XLXI_29 : MUX_20
      port map (CLK=>CLK,
                data1(19 downto 0)=>RAM2_OUTAdderB(19 downto 0),
                data2(19 downto 0)=>RAM1_OUT(19 downto 0),
                s0=>CU_DUMMY(19),
                data_o(19 downto 0)=>MUX1_OUT(19 downto 0));
   
   XLXI_30 : MUX_20
      port map (CLK=>CLK,
                data1(19 downto 0)=>RAM4_OUT(19 downto 0),
                data2(19 downto 0)=>RAM1_OUT(19 downto 0),
                s0=>CU_DUMMY(18),
                data_o(19 downto 0)=>MUX2_OUT(19 downto 0));
   
   XLXI_56 : VCC
      port map (P=>XLXN_55);
   
   XLXI_58 : VCC
      port map (P=>XLXN_57);
   
   XLXI_59 : VCC
      port map (P=>XLXN_59);
   
   XLXI_60 : VCC
      port map (P=>XLXN_61);
   
   XLXI_67 : BUS_8_20_CONV
      port map (data_in(7 downto 0)=>GR_D(7 downto 0),
                data_out(19 downto 0)=>XLXN_180(19 downto 0));
   
   XLXI_69 : BUS_8_20_CONV
      port map (data_in(7 downto 0)=>GR_B(7 downto 0),
                data_out(19 downto 0)=>XLXN_178(19 downto 0));
   
   XLXI_70 : BUS_8_20_CONV
      port map (data_in(7 downto 0)=>GR_A(7 downto 0),
                data_out(19 downto 0)=>XLXN_156(19 downto 0));
   
   XLXI_78 : GR_register20
      port map (clk=>CLK,
                clr=>CU_DUMMY(0),
                d(19 downto 0)=>ADDER_OUT(19 downto 0),
                ld=>CU_DUMMY(1),
                q(19 downto 0)=>GR_RES(19 downto 0));
   
   XLXI_81 : MUX_20
      port map (CLK=>CLK,
                data1(19 downto 0)=>XLXN_178(19 downto 0),
                data2(19 downto 0)=>ADDER_OUT(19 downto 0),
                s0=>CU_DUMMY(16),
                data_o(19 downto 0)=>MUX_4(19 downto 0));
   
end BEHAVIORAL;


