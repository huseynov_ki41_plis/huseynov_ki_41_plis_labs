----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:15:17 12/15/2016 
-- Design Name: 
-- Module Name:    hrn_mod2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hrn_mod2 is
    Port ( HRN_A : in  STD_LOGIC_VECTOR (7 downto 0);
           HRN_out2 : out  STD_LOGIC_VECTOR (15 downto 0));
end hrn_mod2;

architecture Behavioral of hrn_mod2 is
signal hrn_prod,hrn_p7,hrn_p6,hrn_p5,hrn_p4,hrn_p3,hrn_p2,hrn_p1,hrn_p0:unsigned (15 downto 0);
begin
hrn_p1  <="0000000" & unsigned (hrn_A) & "0";
hrn_p2  <="000000" & unsigned (hrn_A) & "00";
hrn_p5  <="000" & unsigned (hrn_A) & "00000";
hrn_p6  <="00" & unsigned (hrn_A) & "000000";
hrn_prod <= (hrn_p1 + hrn_p2 +hrn_p5+ hrn_p6);
hrn_out2 <= std_logic_vector (hrn_prod);

end Behavioral;

