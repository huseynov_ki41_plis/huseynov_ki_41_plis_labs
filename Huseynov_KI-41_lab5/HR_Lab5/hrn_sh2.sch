<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="HRN_A(7:0)" />
        <signal name="XLXN_2(7:0)" />
        <signal name="XLXN_3" />
        <signal name="CLK" />
        <signal name="CE" />
        <signal name="CLR" />
        <signal name="res(15:0)" />
        <signal name="rdy" />
        <signal name="rfd" />
        <signal name="res_ip(17:0)" />
        <port polarity="Input" name="HRN_A(7:0)" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="CE" />
        <port polarity="Input" name="CLR" />
        <port polarity="Output" name="res(15:0)" />
        <port polarity="Output" name="rdy" />
        <port polarity="Output" name="rfd" />
        <port polarity="Output" name="res_ip(17:0)" />
        <blockdef name="hrn_ip">
            <timestamp>2016-12-15T18:38:10</timestamp>
            <rect width="512" x="32" y="32" height="2016" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="1008" y2="1008" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
            <line x2="544" y1="1840" y2="1840" x1="576" />
            <line x2="544" y1="1872" y2="1872" x1="576" />
        </blockdef>
        <blockdef name="hrn_sh">
            <timestamp>2016-12-15T18:39:9</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <block symbolname="hrn_ip" name="XLXI_1">
            <blockpin signalname="HRN_A(7:0)" name="din(7:0)" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="res_ip(17:0)" name="dout(17:0)" />
            <blockpin signalname="rfd" name="rfd" />
            <blockpin signalname="rdy" name="rdy" />
        </block>
        <block symbolname="hrn_sh" name="XLXI_2">
            <blockpin signalname="HRN_A(7:0)" name="HRN_A(7:0)" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="res(15:0)" name="res(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1472" y="416" name="XLXI_1" orien="R0">
        </instance>
        <instance x="784" y="1984" name="XLXI_2" orien="R0">
        </instance>
        <branch name="HRN_A(7:0)">
            <wire x2="656" y1="496" y2="496" x1="496" />
            <wire x2="1472" y1="496" y2="496" x1="656" />
            <wire x2="656" y1="496" y2="1760" x1="656" />
            <wire x2="784" y1="1760" y2="1760" x1="656" />
        </branch>
        <branch name="CLK">
            <wire x2="576" y1="1424" y2="1424" x1="464" />
            <wire x2="1472" y1="1424" y2="1424" x1="576" />
            <wire x2="576" y1="1424" y2="1888" x1="576" />
            <wire x2="784" y1="1888" y2="1888" x1="576" />
        </branch>
        <branch name="CE">
            <wire x2="464" y1="1808" y2="1824" x1="464" />
            <wire x2="784" y1="1824" y2="1824" x1="464" />
        </branch>
        <branch name="CLR">
            <wire x2="784" y1="1952" y2="1952" x1="464" />
        </branch>
        <branch name="res(15:0)">
            <wire x2="1216" y1="1760" y2="1760" x1="1168" />
        </branch>
        <branch name="rdy">
            <wire x2="2304" y1="2288" y2="2288" x1="2048" />
        </branch>
        <branch name="rfd">
            <wire x2="2128" y1="2256" y2="2256" x1="2048" />
        </branch>
        <branch name="res_ip(17:0)">
            <wire x2="2176" y1="496" y2="496" x1="2048" />
        </branch>
        <iomarker fontsize="28" x="496" y="496" name="HRN_A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="464" y="1424" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="464" y="1808" name="CE" orien="R270" />
        <iomarker fontsize="28" x="464" y="1952" name="CLR" orien="R180" />
        <iomarker fontsize="28" x="1216" y="1760" name="res(15:0)" orien="R0" />
        <iomarker fontsize="28" x="2128" y="2256" name="rfd" orien="R0" />
        <iomarker fontsize="28" x="2304" y="2288" name="rdy" orien="R0" />
        <iomarker fontsize="28" x="2176" y="496" name="res_ip(17:0)" orien="R0" />
    </sheet>
</drawing>