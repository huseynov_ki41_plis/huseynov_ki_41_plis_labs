-- Vhdl test bench created from schematic C:\PLIS\HRN_Lab5\hrn_sh2.sch - Thu Dec 15 20:43:23 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY hrn_sh2_hrn_sh2_sch_tb IS
END hrn_sh2_hrn_sh2_sch_tb;
ARCHITECTURE behavioral OF hrn_sh2_hrn_sh2_sch_tb IS 

   COMPONENT hrn_sh2
   PORT( HRN_A	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          CLK	:	IN	STD_LOGIC; 
          CE	:	IN	STD_LOGIC; 
          CLR	:	IN	STD_LOGIC; 
          res	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          rdy	:	OUT	STD_LOGIC; 
          rfd	:	OUT	STD_LOGIC; 
          res_ip	:	OUT	STD_LOGIC_VECTOR (17 DOWNTO 0));
   END COMPONENT;

   SIGNAL HRN_A	:	STD_LOGIC_VECTOR (7 DOWNTO 0) :=x"00";
   SIGNAL CLK	:	STD_LOGIC :='0';
   SIGNAL CE	:	STD_LOGIC :='1';
   SIGNAL CLR	:	STD_LOGIC :='0';
   SIGNAL res	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL rdy	:	STD_LOGIC;
   SIGNAL rfd	:	STD_LOGIC;
   SIGNAL res_ip	:	STD_LOGIC_VECTOR (17 DOWNTO 0);

BEGIN

   UUT: hrn_sh2 PORT MAP(
		HRN_A => HRN_A, 
		CLK => CLK, 
		CE => CE, 
		CLR => CLR, 
		res => res, 
		rdy => rdy, 
		rfd => rfd, 
		res_ip => res_ip
   );
		clk_process : PROCESS
	BEGIN
	CLK <='0';
	WAIT FOR 10 ns;
	CLK <= '1';
	WAIT FOR 10 ns;
	END PROCESS;
	data_change_process : PROCESS
	VARIABLE I : INTEGER;
	BEGIN
	for i in 0 to 3 loop 
	WAIT FOR 20 ns; 
	hrn_A <= conv_std_logic_vector(conv_integer(hrn_A) + 1,8); 
	end loop; 

	hrn_a<="00000000"; 
	for i in 0 to 4 loop 
	wait for 20 ns; 
	end loop; 

	for i in 0 to 3 loop 
	WAIT FOR 20 ns; 
	hrn_A <= conv_std_logic_vector(conv_integer(hrn_A) + 2,8); 
	end loop; 

	hrn_a<="00000000"; 
	for i in 0 to 4 loop 
	wait for 20 ns; 
	end loop; 
	END PROCESS;

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
