The following files were generated for 'hrn_ip' in directory
C:\PLIS\HRN_Lab5\ipcore_dir\

Opens the IP Customization GUI:
   Allows the user to customize or recustomize the IP instance.

   * hrn_ip.mif

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * hrn_ip.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * hrn_ip.ngc
   * hrn_ip.v
   * hrn_ip.veo
   * hrn_ipCOEFF_auto0_0.mif
   * hrn_ipCOEFF_auto0_1.mif
   * hrn_ipCOEFF_auto0_2.mif
   * hrn_ipCOEFF_auto0_3.mif
   * hrn_ipCOEFF_auto0_4.mif
   * hrn_ipCOEFF_auto0_5.mif
   * hrn_ipCOEFF_auto0_6.mif
   * hrn_ipfilt_decode_rom.mif

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * hrn_ip.veo

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * hrn_ip.asy
   * hrn_ip.mif

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * hrn_ip.sym

Generate ISE metadata:
   Create a metadata file for use when including this core in ISE designs

   * hrn_ip_xmdf.tcl

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * hrn_ip.gise
   * hrn_ip.xise

Deliver Readme:
   Readme file for the IP.

   * hrn_ip_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * hrn_ip_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

