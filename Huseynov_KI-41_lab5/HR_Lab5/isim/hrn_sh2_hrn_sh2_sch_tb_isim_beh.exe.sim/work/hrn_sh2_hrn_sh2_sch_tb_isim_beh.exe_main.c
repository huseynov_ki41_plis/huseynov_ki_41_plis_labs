/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *UNISIM_P_0947159679;
char *IEEE_P_2592010699;
char *IEEE_P_3499444699;
char *STD_STANDARD;
char *IEEE_P_3620187407;
char *IEEE_P_1242562249;
char *VL_P_2533777724;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000004134447467_2073120511_init();
    unisims_ver_m_00000000003927721830_1593237687_init();
    unisims_ver_m_00000000003317509437_1759035934_init();
    unisims_ver_m_00000000001508379050_3852734344_init();
    unisims_ver_m_00000000000234138215_2788170701_init();
    unisims_ver_m_00000000000236260522_2449448540_init();
    unisims_ver_m_00000000001108370118_3684891953_init();
    unisims_ver_m_00000000001773747898_2336946039_init();
    unisims_ver_m_00000000001773747898_3056262855_init();
    unisims_ver_m_00000000001773747898_2324208960_init();
    unisims_ver_m_00000000003405408344_3841093685_init();
    unisims_ver_m_00000000002601448656_3367321443_init();
    unisims_ver_m_00000000002661838249_4080905381_init();
    unisims_ver_m_00000000004098988994_0302322055_init();
    unisims_ver_m_00000000000909115699_2771340377_init();
    unisims_ver_m_00000000003848737514_1058825862_init();
    unisims_ver_m_00000000003999256222_3828770576_init();
    unisims_ver_m_00000000003999256222_3461809449_init();
    work_m_00000000000777607984_3922695658_init();
    unisims_ver_m_00000000003286176031_2607553651_init();
    work_m_00000000003434020007_0026147885_init();
    unisims_ver_m_00000000001915777083_3411452309_init();
    unisims_ver_m_00000000003948601558_0484350389_init();
    unisims_ver_m_00000000000924517765_3125220529_init();
    work_m_00000000000733986110_0201105688_init();
    work_m_00000000002003858277_0805412014_init();
    work_m_00000000003459637302_0133437521_init();
    work_m_00000000003000415255_3174834232_init();
    ieee_p_2592010699_init();
    ieee_p_1242562249_init();
    ieee_p_3499444699_init();
    ieee_p_3620187407_init();
    unisim_p_0947159679_init();
    vl_p_2533777724_init();
    work_a_0229654222_3212880686_init();
    work_a_0876413451_3212880686_init();
    work_a_1476818486_3212880686_init();


    xsi_register_tops("work_a_1476818486_3212880686");
    xsi_register_tops("work_m_00000000004134447467_2073120511");

    UNISIM_P_0947159679 = xsi_get_engine_memory("unisim_p_0947159679");
    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    IEEE_P_3499444699 = xsi_get_engine_memory("ieee_p_3499444699");
    STD_STANDARD = xsi_get_engine_memory("std_standard");
    IEEE_P_3620187407 = xsi_get_engine_memory("ieee_p_3620187407");
    IEEE_P_1242562249 = xsi_get_engine_memory("ieee_p_1242562249");
    VL_P_2533777724 = xsi_get_engine_memory("vl_p_2533777724");

    return xsi_run_simulation(argc, argv);

}
