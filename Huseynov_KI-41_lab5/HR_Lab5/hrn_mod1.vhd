----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:10:50 12/15/2016 
-- Design hrne: 
-- Module hrne:    hrn_mod1 - Behavioral 
-- Project hrne: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hrn_mod1 is
    Port ( HRN_A : in  STD_LOGIC_VECTOR (7 downto 0);
           HRN_out1 : out  STD_LOGIC_VECTOR (15 downto 0));
end hrn_mod1;

architecture Behavioral of hrn_mod1 is
signal hrn_prod,hrn_p7,hrn_p6,hrn_p5,hrn_p4,hrn_p3,hrn_p2,hrn_p1,hrn_p0:unsigned (15 downto 0);
begin
hrn_p0  <="00000000" & unsigned (hrn_A) & "";
hrn_p3  <="00000" & unsigned (hrn_A) & "000";
hrn_p7  <="0" & unsigned (hrn_A) & "0000000";
hrn_prod <= (hrn_p0 + hrn_p7) - hrn_p3;
hrn_out1 <= std_logic_vector (hrn_prod);
end Behavioral;

