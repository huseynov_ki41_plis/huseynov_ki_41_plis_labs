<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="HRN_A(7:0)" />
        <signal name="XLXN_3(7:0)" />
        <signal name="XLXN_4(7:0)" />
        <signal name="XLXN_5(7:0)" />
        <signal name="XLXN_7(7:0)" />
        <signal name="XLXN_8(7:0)" />
        <signal name="XLXN_9(7:0)" />
        <signal name="XLXN_10(7:0)" />
        <signal name="XLXN_11(7:0)" />
        <signal name="XLXN_12(7:0)" />
        <signal name="XLXN_13(7:0)" />
        <signal name="XLXN_14(7:0)" />
        <signal name="XLXN_15(7:0)" />
        <signal name="XLXN_17(7:0)" />
        <signal name="CE" />
        <signal name="CLK" />
        <signal name="CLR" />
        <signal name="XLXN_21(15:0)" />
        <signal name="XLXN_22(15:0)" />
        <signal name="XLXN_23(15:0)" />
        <signal name="XLXN_24(15:0)" />
        <signal name="XLXN_25(15:0)" />
        <signal name="XLXN_26(15:0)" />
        <signal name="XLXN_27(15:0)" />
        <signal name="XLXN_28(15:0)" />
        <signal name="XLXN_29(15:0)" />
        <signal name="XLXN_30(15:0)" />
        <signal name="XLXN_31(15:0)" />
        <signal name="XLXN_32(15:0)" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="XLXN_36" />
        <signal name="XLXN_37" />
        <signal name="XLXN_38" />
        <signal name="XLXN_39" />
        <signal name="XLXN_40(15:0)" />
        <signal name="res(15:0)" />
        <signal name="XLXN_42" />
        <signal name="XLXN_43" />
        <signal name="XLXN_44" />
        <signal name="XLXN_45" />
        <signal name="XLXN_46" />
        <signal name="XLXN_47" />
        <signal name="XLXN_48" />
        <signal name="XLXN_49" />
        <port polarity="Input" name="HRN_A(7:0)" />
        <port polarity="Input" name="CE" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="CLR" />
        <port polarity="Output" name="res(15:0)" />
        <blockdef name="hrn_mod1">
            <timestamp>2016-12-15T18:14:36</timestamp>
            <rect width="304" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="hrn_mod2">
            <timestamp>2016-12-15T18:17:57</timestamp>
            <rect width="304" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="fd8ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="fd16ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <block symbolname="hrn_mod1" name="XLXI_1">
            <blockpin signalname="HRN_A(7:0)" name="HRN_A(7:0)" />
            <blockpin signalname="XLXN_21(15:0)" name="HRN_out1(15:0)" />
        </block>
        <block symbolname="hrn_mod1" name="XLXI_2">
            <blockpin signalname="XLXN_4(7:0)" name="HRN_A(7:0)" />
            <blockpin signalname="XLXN_22(15:0)" name="HRN_out1(15:0)" />
        </block>
        <block symbolname="hrn_mod1" name="XLXI_3">
            <blockpin signalname="XLXN_7(7:0)" name="HRN_A(7:0)" />
            <blockpin signalname="XLXN_24(15:0)" name="HRN_out1(15:0)" />
        </block>
        <block symbolname="hrn_mod2" name="XLXI_4">
            <blockpin signalname="XLXN_10(7:0)" name="HRN_A(7:0)" />
            <blockpin signalname="XLXN_26(15:0)" name="HRN_out2(15:0)" />
        </block>
        <block symbolname="hrn_mod2" name="XLXI_5">
            <blockpin signalname="XLXN_12(7:0)" name="HRN_A(7:0)" />
            <blockpin signalname="XLXN_28(15:0)" name="HRN_out2(15:0)" />
        </block>
        <block symbolname="hrn_mod2" name="XLXI_6">
            <blockpin signalname="XLXN_13(7:0)" name="HRN_A(7:0)" />
            <blockpin signalname="XLXN_30(15:0)" name="HRN_out2(15:0)" />
        </block>
        <block symbolname="hrn_mod2" name="XLXI_7">
            <blockpin signalname="XLXN_17(7:0)" name="HRN_A(7:0)" />
            <blockpin signalname="XLXN_32(15:0)" name="HRN_out2(15:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_8">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="HRN_A(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_4(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_9">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_4(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_7(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_10">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_7(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_10(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_11">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_10(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_12(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_12">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_12(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_13(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_13">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_13(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_17(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="add16" name="XLXI_15">
            <blockpin signalname="XLXN_21(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_22(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_39" name="CI" />
            <blockpin signalname="XLXN_38" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_23(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_16">
            <blockpin signalname="XLXN_24(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_23(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_38" name="CI" />
            <blockpin signalname="XLXN_37" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_25(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_17">
            <blockpin signalname="XLXN_26(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_25(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_37" name="CI" />
            <blockpin signalname="XLXN_36" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_27(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_18">
            <blockpin signalname="XLXN_28(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_27(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_36" name="CI" />
            <blockpin signalname="XLXN_35" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_29(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_19">
            <blockpin signalname="XLXN_30(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_29(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_35" name="CI" />
            <blockpin signalname="XLXN_34" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_31(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_20">
            <blockpin signalname="XLXN_32(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_31(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_34" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_40(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_21">
            <blockpin signalname="XLXN_39" name="G" />
        </block>
        <block symbolname="fd16ce" name="XLXI_22">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_40(15:0)" name="D(15:0)" />
            <blockpin signalname="res(15:0)" name="Q(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="1200" y="976" name="XLXI_1" orien="R0">
        </instance>
        <instance x="2592" y="960" name="XLXI_3" orien="R0">
        </instance>
        <instance x="3376" y="960" name="XLXI_4" orien="R0">
        </instance>
        <instance x="4016" y="944" name="XLXI_5" orien="R0">
        </instance>
        <instance x="4672" y="944" name="XLXI_6" orien="R0">
        </instance>
        <instance x="5344" y="944" name="XLXI_7" orien="R0">
        </instance>
        <instance x="1872" y="960" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1040" y="512" name="XLXI_8" orien="R0" />
        <instance x="1904" y="512" name="XLXI_9" orien="R0" />
        <instance x="2672" y="512" name="XLXI_10" orien="R0" />
        <instance x="3424" y="496" name="XLXI_11" orien="R0" />
        <instance x="4128" y="480" name="XLXI_12" orien="R0" />
        <instance x="4768" y="480" name="XLXI_13" orien="R0" />
        <branch name="HRN_A(7:0)">
            <wire x2="896" y1="256" y2="256" x1="784" />
            <wire x2="1040" y1="256" y2="256" x1="896" />
            <wire x2="896" y1="256" y2="944" x1="896" />
            <wire x2="1200" y1="944" y2="944" x1="896" />
        </branch>
        <branch name="XLXN_4(7:0)">
            <wire x2="1728" y1="256" y2="256" x1="1424" />
            <wire x2="1904" y1="256" y2="256" x1="1728" />
            <wire x2="1728" y1="256" y2="928" x1="1728" />
            <wire x2="1872" y1="928" y2="928" x1="1728" />
        </branch>
        <branch name="XLXN_7(7:0)">
            <wire x2="2496" y1="256" y2="256" x1="2288" />
            <wire x2="2672" y1="256" y2="256" x1="2496" />
            <wire x2="2496" y1="256" y2="928" x1="2496" />
            <wire x2="2592" y1="928" y2="928" x1="2496" />
        </branch>
        <branch name="XLXN_10(7:0)">
            <wire x2="3232" y1="256" y2="256" x1="3056" />
            <wire x2="3232" y1="256" y2="928" x1="3232" />
            <wire x2="3376" y1="928" y2="928" x1="3232" />
            <wire x2="3232" y1="240" y2="256" x1="3232" />
            <wire x2="3424" y1="240" y2="240" x1="3232" />
        </branch>
        <branch name="XLXN_12(7:0)">
            <wire x2="3968" y1="240" y2="240" x1="3808" />
            <wire x2="3968" y1="240" y2="912" x1="3968" />
            <wire x2="4016" y1="912" y2="912" x1="3968" />
            <wire x2="3968" y1="224" y2="240" x1="3968" />
            <wire x2="4128" y1="224" y2="224" x1="3968" />
        </branch>
        <branch name="XLXN_13(7:0)">
            <wire x2="4624" y1="224" y2="224" x1="4512" />
            <wire x2="4768" y1="224" y2="224" x1="4624" />
            <wire x2="4624" y1="224" y2="912" x1="4624" />
            <wire x2="4672" y1="912" y2="912" x1="4624" />
        </branch>
        <branch name="XLXN_17(7:0)">
            <wire x2="5248" y1="224" y2="224" x1="5152" />
            <wire x2="5248" y1="224" y2="912" x1="5248" />
            <wire x2="5344" y1="912" y2="912" x1="5248" />
        </branch>
        <branch name="CE">
            <wire x2="864" y1="320" y2="320" x1="800" />
            <wire x2="1040" y1="320" y2="320" x1="864" />
            <wire x2="864" y1="320" y2="576" x1="864" />
            <wire x2="864" y1="576" y2="1728" x1="864" />
            <wire x2="1440" y1="576" y2="576" x1="864" />
            <wire x2="2368" y1="576" y2="576" x1="1440" />
            <wire x2="3120" y1="576" y2="576" x1="2368" />
            <wire x2="3872" y1="576" y2="576" x1="3120" />
            <wire x2="4576" y1="576" y2="576" x1="3872" />
            <wire x2="816" y1="1728" y2="1808" x1="816" />
            <wire x2="6256" y1="1808" y2="1808" x1="816" />
            <wire x2="864" y1="1728" y2="1728" x1="816" />
            <wire x2="1440" y1="320" y2="576" x1="1440" />
            <wire x2="1904" y1="320" y2="320" x1="1440" />
            <wire x2="2368" y1="320" y2="576" x1="2368" />
            <wire x2="2672" y1="320" y2="320" x1="2368" />
            <wire x2="3120" y1="304" y2="576" x1="3120" />
            <wire x2="3424" y1="304" y2="304" x1="3120" />
            <wire x2="3872" y1="288" y2="576" x1="3872" />
            <wire x2="4128" y1="288" y2="288" x1="3872" />
            <wire x2="4576" y1="288" y2="576" x1="4576" />
            <wire x2="4768" y1="288" y2="288" x1="4576" />
        </branch>
        <branch name="CLK">
            <wire x2="944" y1="384" y2="384" x1="848" />
            <wire x2="1040" y1="384" y2="384" x1="944" />
            <wire x2="944" y1="384" y2="640" x1="944" />
            <wire x2="944" y1="640" y2="1840" x1="944" />
            <wire x2="992" y1="1840" y2="1840" x1="944" />
            <wire x2="992" y1="1840" y2="1872" x1="992" />
            <wire x2="6256" y1="1872" y2="1872" x1="992" />
            <wire x2="1456" y1="640" y2="640" x1="944" />
            <wire x2="2352" y1="640" y2="640" x1="1456" />
            <wire x2="3136" y1="640" y2="640" x1="2352" />
            <wire x2="3856" y1="640" y2="640" x1="3136" />
            <wire x2="4560" y1="640" y2="640" x1="3856" />
            <wire x2="1456" y1="384" y2="640" x1="1456" />
            <wire x2="1904" y1="384" y2="384" x1="1456" />
            <wire x2="2352" y1="384" y2="640" x1="2352" />
            <wire x2="2672" y1="384" y2="384" x1="2352" />
            <wire x2="3136" y1="368" y2="640" x1="3136" />
            <wire x2="3424" y1="368" y2="368" x1="3136" />
            <wire x2="3856" y1="352" y2="640" x1="3856" />
            <wire x2="4128" y1="352" y2="352" x1="3856" />
            <wire x2="4560" y1="352" y2="640" x1="4560" />
            <wire x2="4768" y1="352" y2="352" x1="4560" />
        </branch>
        <branch name="CLR">
            <wire x2="1008" y1="480" y2="480" x1="816" />
            <wire x2="1040" y1="480" y2="480" x1="1008" />
            <wire x2="1008" y1="480" y2="496" x1="1008" />
            <wire x2="1040" y1="496" y2="496" x1="1008" />
            <wire x2="1040" y1="496" y2="688" x1="1040" />
            <wire x2="1040" y1="688" y2="1936" x1="1040" />
            <wire x2="1040" y1="1936" y2="1968" x1="1040" />
            <wire x2="6256" y1="1968" y2="1968" x1="1040" />
            <wire x2="1904" y1="688" y2="688" x1="1040" />
            <wire x2="2672" y1="688" y2="688" x1="1904" />
            <wire x2="3424" y1="688" y2="688" x1="2672" />
            <wire x2="4128" y1="688" y2="688" x1="3424" />
            <wire x2="4768" y1="688" y2="688" x1="4128" />
            <wire x2="1904" y1="480" y2="688" x1="1904" />
            <wire x2="2672" y1="480" y2="688" x1="2672" />
            <wire x2="3424" y1="464" y2="688" x1="3424" />
            <wire x2="4128" y1="448" y2="688" x1="4128" />
            <wire x2="4768" y1="448" y2="688" x1="4768" />
        </branch>
        <iomarker fontsize="28" x="784" y="256" name="HRN_A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="800" y="320" name="CE" orien="R180" />
        <iomarker fontsize="28" x="848" y="384" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="816" y="480" name="CLR" orien="R180" />
        <instance x="1568" y="1728" name="XLXI_15" orien="R0" />
        <instance x="2784" y="1728" name="XLXI_16" orien="R0" />
        <instance x="3696" y="1680" name="XLXI_17" orien="R0" />
        <instance x="4496" y="1664" name="XLXI_18" orien="R0" />
        <instance x="5280" y="1648" name="XLXI_19" orien="R0" />
        <instance x="5920" y="1600" name="XLXI_20" orien="R0" />
        <branch name="XLXN_21(15:0)">
            <wire x2="1504" y1="1200" y2="1408" x1="1504" />
            <wire x2="1568" y1="1408" y2="1408" x1="1504" />
            <wire x2="1680" y1="1200" y2="1200" x1="1504" />
            <wire x2="1680" y1="944" y2="944" x1="1632" />
            <wire x2="1680" y1="944" y2="1200" x1="1680" />
        </branch>
        <branch name="XLXN_22(15:0)">
            <wire x2="1440" y1="1152" y2="1536" x1="1440" />
            <wire x2="1568" y1="1536" y2="1536" x1="1440" />
            <wire x2="2384" y1="1152" y2="1152" x1="1440" />
            <wire x2="2384" y1="928" y2="928" x1="2304" />
            <wire x2="2384" y1="928" y2="1152" x1="2384" />
        </branch>
        <branch name="XLXN_23(15:0)">
            <wire x2="2400" y1="1472" y2="1472" x1="2016" />
            <wire x2="2400" y1="1472" y2="1536" x1="2400" />
            <wire x2="2784" y1="1536" y2="1536" x1="2400" />
        </branch>
        <branch name="XLXN_24(15:0)">
            <wire x2="2736" y1="1216" y2="1408" x1="2736" />
            <wire x2="2784" y1="1408" y2="1408" x1="2736" />
            <wire x2="3104" y1="1216" y2="1216" x1="2736" />
            <wire x2="3104" y1="928" y2="928" x1="3024" />
            <wire x2="3104" y1="928" y2="1216" x1="3104" />
        </branch>
        <branch name="XLXN_25(15:0)">
            <wire x2="3456" y1="1472" y2="1472" x1="3232" />
            <wire x2="3456" y1="1472" y2="1488" x1="3456" />
            <wire x2="3696" y1="1488" y2="1488" x1="3456" />
        </branch>
        <branch name="XLXN_26(15:0)">
            <wire x2="3616" y1="1152" y2="1360" x1="3616" />
            <wire x2="3696" y1="1360" y2="1360" x1="3616" />
            <wire x2="3888" y1="1152" y2="1152" x1="3616" />
            <wire x2="3888" y1="928" y2="928" x1="3808" />
            <wire x2="3888" y1="928" y2="1152" x1="3888" />
        </branch>
        <branch name="XLXN_27(15:0)">
            <wire x2="4320" y1="1424" y2="1424" x1="4144" />
            <wire x2="4320" y1="1424" y2="1472" x1="4320" />
            <wire x2="4496" y1="1472" y2="1472" x1="4320" />
        </branch>
        <branch name="XLXN_28(15:0)">
            <wire x2="4464" y1="912" y2="912" x1="4448" />
            <wire x2="4464" y1="912" y2="1344" x1="4464" />
            <wire x2="4496" y1="1344" y2="1344" x1="4464" />
        </branch>
        <branch name="XLXN_29(15:0)">
            <wire x2="5104" y1="1408" y2="1408" x1="4944" />
            <wire x2="5104" y1="1408" y2="1456" x1="5104" />
            <wire x2="5280" y1="1456" y2="1456" x1="5104" />
        </branch>
        <branch name="XLXN_30(15:0)">
            <wire x2="5184" y1="912" y2="912" x1="5104" />
            <wire x2="5184" y1="912" y2="1328" x1="5184" />
            <wire x2="5280" y1="1328" y2="1328" x1="5184" />
        </branch>
        <branch name="XLXN_31(15:0)">
            <wire x2="5824" y1="1392" y2="1392" x1="5728" />
            <wire x2="5824" y1="1392" y2="1408" x1="5824" />
            <wire x2="5920" y1="1408" y2="1408" x1="5824" />
        </branch>
        <branch name="XLXN_32(15:0)">
            <wire x2="5840" y1="912" y2="912" x1="5776" />
            <wire x2="5840" y1="912" y2="1280" x1="5840" />
            <wire x2="5920" y1="1280" y2="1280" x1="5840" />
        </branch>
        <branch name="XLXN_34">
            <wire x2="5808" y1="1584" y2="1584" x1="5728" />
            <wire x2="5808" y1="1152" y2="1584" x1="5808" />
            <wire x2="5920" y1="1152" y2="1152" x1="5808" />
        </branch>
        <branch name="XLXN_35">
            <wire x2="5088" y1="1600" y2="1600" x1="4944" />
            <wire x2="5088" y1="1200" y2="1600" x1="5088" />
            <wire x2="5280" y1="1200" y2="1200" x1="5088" />
        </branch>
        <branch name="XLXN_36">
            <wire x2="4304" y1="1616" y2="1616" x1="4144" />
            <wire x2="4304" y1="1216" y2="1616" x1="4304" />
            <wire x2="4496" y1="1216" y2="1216" x1="4304" />
        </branch>
        <branch name="XLXN_37">
            <wire x2="3440" y1="1664" y2="1664" x1="3232" />
            <wire x2="3440" y1="1232" y2="1664" x1="3440" />
            <wire x2="3696" y1="1232" y2="1232" x1="3440" />
        </branch>
        <branch name="XLXN_38">
            <wire x2="2384" y1="1664" y2="1664" x1="2016" />
            <wire x2="2384" y1="1280" y2="1664" x1="2384" />
            <wire x2="2784" y1="1280" y2="1280" x1="2384" />
        </branch>
        <branch name="XLXN_39">
            <wire x2="1568" y1="1280" y2="1280" x1="1328" />
            <wire x2="1328" y1="1280" y2="1392" x1="1328" />
            <wire x2="1328" y1="1392" y2="1504" x1="1328" />
        </branch>
        <instance x="1264" y="1632" name="XLXI_21" orien="R0" />
        <instance x="6256" y="2000" name="XLXI_22" orien="R0" />
        <branch name="XLXN_40(15:0)">
            <wire x2="6176" y1="1600" y2="1744" x1="6176" />
            <wire x2="6256" y1="1744" y2="1744" x1="6176" />
            <wire x2="6448" y1="1600" y2="1600" x1="6176" />
            <wire x2="6448" y1="1344" y2="1344" x1="6368" />
            <wire x2="6448" y1="1344" y2="1600" x1="6448" />
        </branch>
        <branch name="res(15:0)">
            <wire x2="6720" y1="1744" y2="1744" x1="6640" />
        </branch>
        <iomarker fontsize="28" x="6720" y="1744" name="res(15:0)" orien="R0" />
    </sheet>
</drawing>