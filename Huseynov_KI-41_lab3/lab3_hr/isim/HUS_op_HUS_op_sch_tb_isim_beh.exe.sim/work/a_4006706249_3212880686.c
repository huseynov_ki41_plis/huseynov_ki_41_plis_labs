/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/lab3_hr/HUS_C_mM.vhd";
extern char *IEEE_P_3499444699;

char *ieee_p_3499444699_sub_2254111597_3536714472(char *, char *, char *, char *, char *, char *);
char *ieee_p_3499444699_sub_2254183471_3536714472(char *, char *, char *, char *, char *, char *);


static void work_a_4006706249_3212880686_p_0(char *t0)
{
    char t5[16];
    char t7[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    unsigned char t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(14, ng0);

LAB3:    t1 = (t0 + 10164);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 31;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (31 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9700U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t11 = (32U + 8U);
    t12 = (40U != t11);
    if (t12 == 1)
        goto LAB5;

LAB6:    t13 = (t0 + 6992);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    memcpy(t17, t3, 40U);
    xsi_driver_first_trans_fast(t13);

LAB2:    t18 = (t0 + 6752);
    *((int *)t18) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t11, 0);
    goto LAB6;

}

static void work_a_4006706249_3212880686_p_1(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(23, ng0);

LAB3:    t1 = (t0 + 10196);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 22;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (22 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9700U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10219);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 8;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (8 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (23U + 8U);
    t21 = (t11 + 9U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7056);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6768);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_4006706249_3212880686_p_2(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(27, ng0);

LAB3:    t1 = (t0 + 10228);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 18;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (18 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9700U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10247);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 12;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (12 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (19U + 8U);
    t21 = (t11 + 13U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7120);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6784);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_4006706249_3212880686_p_3(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(29, ng0);

LAB3:    t1 = (t0 + 10260);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 16;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (16 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9700U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10277);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 14;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (14 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (17U + 8U);
    t21 = (t11 + 15U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7184);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6800);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_4006706249_3212880686_p_4(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(31, ng0);

LAB3:    t1 = (t0 + 10292);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 14;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (14 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9700U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10307);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 16;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (16 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (15U + 8U);
    t21 = (t11 + 17U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7248);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6816);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_4006706249_3212880686_p_5(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(35, ng0);

LAB3:    t1 = (t0 + 10324);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 10;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (10 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9700U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10335);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 20;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (20 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (11U + 8U);
    t21 = (t11 + 21U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7312);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6832);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_4006706249_3212880686_p_6(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(37, ng0);

LAB3:    t1 = (t0 + 10356);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 8;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (8 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9700U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10365);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 22;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (22 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (9U + 8U);
    t21 = (t11 + 23U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7376);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6848);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_4006706249_3212880686_p_7(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(39, ng0);

LAB3:    t1 = (t0 + 10388);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 6;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (6 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9700U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10395);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 24;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (24 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (7U + 8U);
    t21 = (t11 + 25U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7440);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6864);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_4006706249_3212880686_p_8(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(42, ng0);

LAB3:    t1 = (t0 + 10420);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 3;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (3 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9700U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10424);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 27;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (27 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (4U + 8U);
    t21 = (t11 + 28U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7504);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6880);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_4006706249_3212880686_p_9(char *t0)
{
    char t1[16];
    char t2[16];
    char t3[16];
    char t4[16];
    char t5[16];
    char t6[16];
    char t7[16];
    char t8[16];
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    unsigned int t36;
    unsigned int t37;
    unsigned char t38;
    char *t39;
    char *t40;
    char *t41;
    char *t42;
    char *t43;
    char *t44;

LAB0:    xsi_set_current_line(47, ng0);

LAB3:    t9 = (t0 + 1512U);
    t10 = *((char **)t9);
    t9 = (t0 + 9732U);
    t11 = (t0 + 1672U);
    t12 = *((char **)t11);
    t11 = (t0 + 9732U);
    t13 = ieee_p_3499444699_sub_2254111597_3536714472(IEEE_P_3499444699, t8, t10, t9, t12, t11);
    t14 = (t0 + 1832U);
    t15 = *((char **)t14);
    t14 = (t0 + 9732U);
    t16 = ieee_p_3499444699_sub_2254111597_3536714472(IEEE_P_3499444699, t7, t13, t8, t15, t14);
    t17 = (t0 + 1992U);
    t18 = *((char **)t17);
    t17 = (t0 + 9732U);
    t19 = ieee_p_3499444699_sub_2254111597_3536714472(IEEE_P_3499444699, t6, t16, t7, t18, t17);
    t20 = (t0 + 2152U);
    t21 = *((char **)t20);
    t20 = (t0 + 9732U);
    t22 = ieee_p_3499444699_sub_2254111597_3536714472(IEEE_P_3499444699, t5, t19, t6, t21, t20);
    t23 = (t0 + 2312U);
    t24 = *((char **)t23);
    t23 = (t0 + 9732U);
    t25 = ieee_p_3499444699_sub_2254111597_3536714472(IEEE_P_3499444699, t4, t22, t5, t24, t23);
    t26 = (t0 + 2472U);
    t27 = *((char **)t26);
    t26 = (t0 + 9732U);
    t28 = ieee_p_3499444699_sub_2254111597_3536714472(IEEE_P_3499444699, t3, t25, t4, t27, t26);
    t29 = (t0 + 2632U);
    t30 = *((char **)t29);
    t29 = (t0 + 9732U);
    t31 = ieee_p_3499444699_sub_2254111597_3536714472(IEEE_P_3499444699, t2, t28, t3, t30, t29);
    t32 = (t0 + 2792U);
    t33 = *((char **)t32);
    t32 = (t0 + 9732U);
    t34 = ieee_p_3499444699_sub_2254183471_3536714472(IEEE_P_3499444699, t1, t31, t2, t33, t32);
    t35 = (t1 + 12U);
    t36 = *((unsigned int *)t35);
    t37 = (1U * t36);
    t38 = (40U != t37);
    if (t38 == 1)
        goto LAB5;

LAB6:    t39 = (t0 + 7568);
    t40 = (t39 + 56U);
    t41 = *((char **)t40);
    t42 = (t41 + 56U);
    t43 = *((char **)t42);
    memcpy(t43, t34, 40U);
    xsi_driver_first_trans_fast(t39);

LAB2:    t44 = (t0 + 6896);
    *((int *)t44) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t37, 0);
    goto LAB6;

}

static void work_a_4006706249_3212880686_p_10(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(49, ng0);

LAB3:    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t1 = (t0 + 7632);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 40U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 6912);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_4006706249_3212880686_init()
{
	static char *pe[] = {(void *)work_a_4006706249_3212880686_p_0,(void *)work_a_4006706249_3212880686_p_1,(void *)work_a_4006706249_3212880686_p_2,(void *)work_a_4006706249_3212880686_p_3,(void *)work_a_4006706249_3212880686_p_4,(void *)work_a_4006706249_3212880686_p_5,(void *)work_a_4006706249_3212880686_p_6,(void *)work_a_4006706249_3212880686_p_7,(void *)work_a_4006706249_3212880686_p_8,(void *)work_a_4006706249_3212880686_p_9,(void *)work_a_4006706249_3212880686_p_10};
	xsi_register_didat("work_a_4006706249_3212880686", "isim/HUS_op_HUS_op_sch_tb_isim_beh.exe.sim/work/a_4006706249_3212880686.didat");
	xsi_register_executes(pe);
}
