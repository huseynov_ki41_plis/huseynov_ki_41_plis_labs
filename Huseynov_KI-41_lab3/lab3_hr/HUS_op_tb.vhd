
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY HUS_op_HUS_op_sch_tb IS
END HUS_op_HUS_op_sch_tb;
ARCHITECTURE behavioral OF HUS_op_HUS_op_sch_tb IS 

   COMPONENT HUS_op
   PORT( HUS_A	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          HUS_M	:	OUT	STD_LOGIC_VECTOR (39 DOWNTO 0); 
          HUS_mM	:	OUT	STD_LOGIC_VECTOR (39 DOWNTO 0); 
          HUS_IPA	:	OUT	STD_LOGIC_VECTOR (39 DOWNTO 0));
   END COMPONENT;

   SIGNAL HUS_A	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL HUS_M	:	STD_LOGIC_VECTOR (39 DOWNTO 0);
   SIGNAL HUS_mM	:	STD_LOGIC_VECTOR (39 DOWNTO 0);
   SIGNAL HUS_IPA	:	STD_LOGIC_VECTOR (39 DOWNTO 0);

BEGIN

   UUT: HUS_op PORT MAP(
		HUS_A => HUS_A, 
		HUS_M => HUS_M, 
		HUS_mM => HUS_mM, 
		HUS_IPA => HUS_IPA
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
	HUS_A<= "00000011";
for i in 1 to 20 loop wait for 5ns;
HUS_A <= std_logic_vector(unsigned(HUS_A) + (1)); wait for 5 ns;
end loop;
	
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
