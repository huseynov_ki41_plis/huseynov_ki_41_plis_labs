--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : HUS_op_drc.vhf
-- /___/   /\     Timestamp : 12/01/2016 02:45:40
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: C:\Xilinx\14.7\ISE_DS\ISE\bin\nt64\unwrapped\sch2hdl.exe -sympath D:/lab3_hr/ipcore_dir -intstyle ise -family virtex4 -flat -suppress -vhdl HUS_op_drc.vhf -w D:/lab3_hr/HUS_op.sch
--Design Name: HUS_op
--Device: virtex4
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity HUS_op is
   port ( HUS_A   : in    std_logic_vector (7 downto 0); 
          HUS_IPA : out   std_logic_vector (39 downto 0); 
          HUS_M   : out   std_logic_vector (39 downto 0); 
          HUS_mM  : out   std_logic_vector (39 downto 0));
end HUS_op;

architecture BEHAVIORAL of HUS_op is
   component Hus_op_ip
      port ( a : in    std_logic_vector (7 downto 0); 
             p : out   std_logic_vector (39 downto 0));
   end component;
   
   component HUS_C_MULT
      port ( HUS_A   : in    std_logic_vector (7 downto 0); 
             HUS_C_M : out   std_logic_vector (39 downto 0));
   end component;
   
   component HUS_C_mM
      port ( HUS_A    : in    std_logic_vector (7 downto 0); 
             HUS_C_mM : out   std_logic_vector (39 downto 0));
   end component;
   
begin
   XLXI_7 : Hus_op_ip
      port map (a(7 downto 0)=>HUS_A(7 downto 0),
                p(39 downto 0)=>HUS_IPA(39 downto 0));
   
   XLXI_8 : HUS_C_MULT
      port map (HUS_A(7 downto 0)=>HUS_A(7 downto 0),
                HUS_C_M(39 downto 0)=>HUS_M(39 downto 0));
   
   XLXI_9 : HUS_C_mM
      port map (HUS_A(7 downto 0)=>HUS_A(7 downto 0),
                HUS_C_mM(39 downto 0)=>HUS_mM(39 downto 0));
   
end BEHAVIORAL;


