library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
entity HUS_C_mM is
Port ( HUS_A : in STD_LOGIC_VECTOR (7 downto 0); HUS_C_mM : out STD_LOGIC_VECTOR (39 downto 0));


end HUS_C_mM;

architecture Behavioral of HUS_C_mM is
signal hus_prod,hus_p0,hus_p9,hus_p13,hus_p15,hus_p17,hus_p21,hus_p23,hus_p25,hus_p28:unsigned(39 downto 0);
begin
hus_p0 <= "00000000000000000000000000000000" & unsigned(HUS_A);
--hus_p1 <= "0000000000000000000000000000000" & unsigned(HUS_A) & "0";
--hus_p2 <= "000000000000000000000000000000" & unsigned(HUS_A) & "00";
--hus_p3 <= "00000000000000000000000000000" & unsigned(HUS_A) & "000";
--hus_p4 <= "0000000000000000000000000000" & unsigned(HUS_A) & "0000";
--hus_p5 <= "000000000000000000000000000" & unsigned(HUS_A) & "00000";
--hus_p6 <= "00000000000000000000000000" & unsigned(HUS_A) & "000000";
--hus_p7 <= "0000000000000000000000000" & unsigned(HUS_A) & "0000000";
--hus_p8 <= "000000000000000000000000" & unsigned(HUS_A) & "00000000";
hus_p9 <= "00000000000000000000000" & unsigned(HUS_A) & "000000000";
--hus_p10 <= "0000000000000000000000" & unsigned(HUS_A) & "0000000000";
--hus_p11 <= "000000000000000000000" & unsigned(HUS_A) & "00000000000";
--hus_p12 <= "00000000000000000000" & unsigned(HUS_A) & "000000000000";
hus_p13 <= "0000000000000000000" & unsigned(HUS_A) & "0000000000000";
--hus_p14 <= "000000000000000000" & unsigned(HUS_A) & "00000000000000";
hus_p15 <= "00000000000000000" & unsigned(HUS_A) & "000000000000000";
--hus_p16 <= "0000000000000000" & unsigned(HUS_A) & "0000000000000000";
hus_p17 <= "000000000000000" & unsigned(HUS_A) & "00000000000000000";
--hus_p18 <= "00000000000000" & unsigned(HUS_A) & "000000000000000000";
--hus_p19 <= "0000000000000" & unsigned(HUS_A) & "0000000000000000000";
--hus_p20 <= "000000000000" & unsigned(HUS_A) & "00000000000000000000";
hus_p21 <= "00000000000" & unsigned(HUS_A) & "000000000000000000000";
--hus_p22 <= "0000000000" & unsigned(HUS_A) & "0000000000000000000000";
hus_p23 <= "000000000" & unsigned(HUS_A) & "00000000000000000000000";
--hus_p24 <= "00000000" & unsigned(HUS_A) & "000000000000000000000000";
hus_p25 <= "0000000" & unsigned(HUS_A) & "0000000000000000000000000";
--hus_p26 <= "000000" & unsigned(HUS_A) & "00000000000000000000000000";
--hus_p27 <= "00000" & unsigned(HUS_A) & "000000000000000000000000000";
hus_p28 <= "0000" & unsigned(HUS_A) & "0000000000000000000000000000";
--hus_p29 <= "000" & unsigned(HUS_A) & "00000000000000000000000000000";
--hus_p30 <= "00" & unsigned(HUS_A) & "000000000000000000000000000000";
--hus_p31 <= "0" & unsigned(HUS_A) & "0000000000000000000000000000000";

hus_prod<=hus_p0+hus_p9+hus_p13+hus_p15+hus_p17+hus_p21+hus_p23+hus_p25-hus_p28;

HUS_C_mM <= std_logic_vector (hus_prod);
end Behavioral;