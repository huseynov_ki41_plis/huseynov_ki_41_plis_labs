<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_3(17:0)" />
        <signal name="HUS_A(7:0)" />
        <signal name="XLXN_9(7:0)" />
        <signal name="HUS_M(39:0)" />
        <signal name="HUS_mM(39:0)" />
        <signal name="HUS_IPA(39:0)" />
        <port polarity="Input" name="HUS_A(7:0)" />
        <port polarity="Output" name="HUS_M(39:0)" />
        <port polarity="Output" name="HUS_mM(39:0)" />
        <port polarity="Output" name="HUS_IPA(39:0)" />
        <blockdef name="HUS_C_MULT">
            <timestamp>2016-11-16T18:54:30</timestamp>
            <rect width="320" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="384" y="-44" height="24" />
            <line x2="448" y1="-32" y2="-32" x1="384" />
        </blockdef>
        <blockdef name="HUS_C_mM">
            <timestamp>2016-11-30T23:52:14</timestamp>
            <rect width="336" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="400" y="-44" height="24" />
            <line x2="464" y1="-32" y2="-32" x1="400" />
        </blockdef>
        <blockdef name="Hus_op_ip">
            <timestamp>2016-12-1T0:35:59</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <block symbolname="Hus_op_ip" name="XLXI_7">
            <blockpin signalname="HUS_A(7:0)" name="a(7:0)" />
            <blockpin signalname="HUS_IPA(39:0)" name="p(39:0)" />
        </block>
        <block symbolname="HUS_C_MULT" name="XLXI_8">
            <blockpin signalname="HUS_A(7:0)" name="HUS_A(7:0)" />
            <blockpin signalname="HUS_M(39:0)" name="HUS_C_M(39:0)" />
        </block>
        <block symbolname="HUS_C_mM" name="XLXI_9">
            <blockpin signalname="HUS_A(7:0)" name="HUS_A(7:0)" />
            <blockpin signalname="HUS_mM(39:0)" name="HUS_C_mM(39:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1392" y="1792" name="XLXI_7" orien="R0">
        </instance>
        <instance x="1440" y="1344" name="XLXI_8" orien="R0">
        </instance>
        <instance x="1440" y="1616" name="XLXI_9" orien="R0">
        </instance>
        <branch name="HUS_A(7:0)">
            <wire x2="1280" y1="1312" y2="1312" x1="1216" />
            <wire x2="1440" y1="1312" y2="1312" x1="1280" />
            <wire x2="1280" y1="1312" y2="1584" x1="1280" />
            <wire x2="1280" y1="1584" y2="1872" x1="1280" />
            <wire x2="1392" y1="1872" y2="1872" x1="1280" />
            <wire x2="1440" y1="1584" y2="1584" x1="1280" />
        </branch>
        <branch name="HUS_M(39:0)">
            <wire x2="2224" y1="1312" y2="1312" x1="1888" />
        </branch>
        <branch name="HUS_mM(39:0)">
            <wire x2="2240" y1="1584" y2="1584" x1="1904" />
        </branch>
        <branch name="HUS_IPA(39:0)">
            <wire x2="2240" y1="1872" y2="1872" x1="1968" />
        </branch>
        <iomarker fontsize="28" x="1216" y="1312" name="HUS_A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="2224" y="1312" name="HUS_M(39:0)" orien="R0" />
        <iomarker fontsize="28" x="2240" y="1584" name="HUS_mM(39:0)" orien="R0" />
        <iomarker fontsize="28" x="2240" y="1872" name="HUS_IPA(39:0)" orien="R0" />
    </sheet>
</drawing>