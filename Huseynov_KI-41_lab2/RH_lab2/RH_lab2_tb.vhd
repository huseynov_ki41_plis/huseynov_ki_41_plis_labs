-- Vhdl test bench created from schematic D:\RH_lab2\GVV_ALL.sch - Fri Oct 14 16:33:43 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY GVV_ALL_GVV_ALL_sch_tb IS
END GVV_ALL_GVV_ALL_sch_tb;
ARCHITECTURE behavioral OF GVV_ALL_GVV_ALL_sch_tb IS 

   COMPONENT GVV_ALL
   PORT( gvv_B	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          gvv_prod	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          gvv_mPROD	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          gvvv_IP	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          gvv_A	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0));
   END COMPONENT;

   SIGNAL gvv_B	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL gvv_prod	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL gvv_mPROD	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL gvvv_IP	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL gvv_A	:	STD_LOGIC_VECTOR (7 DOWNTO 0);

BEGIN

   UUT: GVV_ALL PORT MAP(
		gvv_B => gvv_B, 
		gvv_prod => gvv_prod, 
		gvv_mPROD => gvv_mPROD, 
		gvvv_IP => gvvv_IP, 
		gvv_A => gvv_A
   );

 

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
	
gvv_A <= "11101011";
gvv_B <= "11101011";

wait for 5ns;
for i in 1 to 20 loop
gvv_A <= std_logic_vector(unsigned(gvv_A) + (1));
gvv_B <= std_logic_vector(unsigned(gvv_B) + (1));


wait for 5ns;

end loop;
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
