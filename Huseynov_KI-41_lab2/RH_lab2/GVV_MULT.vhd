library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;
-- Uncomment the following library declaration if
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
entity GVV_MULT is
Port ( GVV_A : in STD_LOGIC_VECTOR (7 downto 0);
GVV_B : in STD_LOGIC_VECTOR (7 downto 0);
GVV_PROD : out STD_LOGIC_VECTOR (15 downto 0));
end GVV_MULT;
architecture Behavioral of GVV_MULT is
constant WIDTH: integer:=8;
signal gvv_ua, gvv_bv0, gvv_bv1, gvv_bv2, gvv_bv3, gvv_bv4, gvv_bv5, gvv_bv6, gvv_bv7 : unsigned (WIDTH - 1 downto 0);
signal gvv_p, gvv_p0, gvv_p1, gvv_p2, gvv_p3, gvv_p4, gvv_p5, gvv_p6, gvv_p7 : unsigned (2*WIDTH - 1 downto 0);
begin
gvv_ua <= unsigned (GVV_A);
gvv_bv0 <= (others => GVV_B(0));
gvv_bv1 <= (others => GVV_B(1));
gvv_bv2 <= (others => GVV_B(2));
gvv_bv3 <= (others => GVV_B(3));
gvv_bv4 <= (others => GVV_B(4));
gvv_bv5 <= (others => GVV_B(5));
gvv_bv6 <= (others => GVV_B(6));
gvv_bv7 <= (others => GVV_B(7));
gvv_p0 <= "00000000" & (gvv_bv0 and gvv_ua);
gvv_p1 <= "0000000" & (gvv_bv1 and gvv_ua) & "0";
gvv_p2 <= "000000" & (gvv_bv2 and gvv_ua) & "00";
gvv_p3 <= "00000" & (gvv_bv3 and gvv_ua) & "000";
gvv_p4 <= "0000" & (gvv_bv4 and gvv_ua) & "0000";
gvv_p5 <= "000" & (gvv_bv5 and gvv_ua) & "00000";
gvv_p6 <= "00" & (gvv_bv6 and gvv_ua) & "000000";
gvv_p7 <= "0" & (gvv_bv7 and gvv_ua) & "0000000";
gvv_p<=((gvv_p0+gvv_p1)+(gvv_p2+gvv_p3))+((gvv_p4+gvv_p5)+(gvv_p6+gvv_p7));
GVV_PROD<= std_logic_vector (gvv_p);
end Behavioral;