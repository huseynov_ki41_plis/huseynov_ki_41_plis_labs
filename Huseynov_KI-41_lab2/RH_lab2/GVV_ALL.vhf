--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : GVV_ALL.vhf
-- /___/   /\     Timestamp : 10/15/2016 21:16:26
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -sympath D:/RH_lab2/ipcore_dir -intstyle ise -family virtex4 -flat -suppress -vhdl D:/RH_lab2/GVV_ALL.vhf -w D:/RH_lab2/GVV_ALL.sch
--Design Name: GVV_ALL
--Device: virtex4
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity GVV_ALL is
   port ( gvv_A     : in    std_logic_vector (7 downto 0); 
          gvv_B     : in    std_logic_vector (7 downto 0); 
          gvvv_IP   : out   std_logic_vector (15 downto 0); 
          gvv_mPROD : out   std_logic_vector (15 downto 0); 
          gvv_prod  : out   std_logic_vector (15 downto 0));
end GVV_ALL;

architecture BEHAVIORAL of GVV_ALL is
   component GVV_MULT
      port ( GVV_A    : in    std_logic_vector (7 downto 0); 
             GVV_B    : in    std_logic_vector (7 downto 0); 
             GVV_PROD : out   std_logic_vector (15 downto 0));
   end component;
   
   component GVV_mod_multiplier
      port ( GVV_A     : in    std_logic_vector (7 downto 0); 
             GVV_B     : in    std_logic_vector (7 downto 0); 
             GVV_mPROD : out   std_logic_vector (15 downto 0));
   end component;
   
   component GVV_IP
      port ( a : in    std_logic_vector (7 downto 0); 
             b : in    std_logic_vector (7 downto 0); 
             p : out   std_logic_vector (15 downto 0));
   end component;
   
begin
   XLXI_1 : GVV_MULT
      port map (GVV_A(7 downto 0)=>gvv_A(7 downto 0),
                GVV_B(7 downto 0)=>gvv_B(7 downto 0),
                GVV_PROD(15 downto 0)=>gvv_prod(15 downto 0));
   
   XLXI_2 : GVV_mod_multiplier
      port map (GVV_A(7 downto 0)=>gvv_A(7 downto 0),
                GVV_B(7 downto 0)=>gvv_B(7 downto 0),
                GVV_mPROD(15 downto 0)=>gvv_mPROD(15 downto 0));
   
   XLXI_3 : GVV_IP
      port map (a(7 downto 0)=>gvv_A(7 downto 0),
                b(7 downto 0)=>gvv_B(7 downto 0),
                p(15 downto 0)=>gvvv_IP(15 downto 0));
   
end BEHAVIORAL;


