--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: HR_lab1_mod_synthesis.vhd
-- /___/   /\     Timestamp: Thu Oct 13 11:08:51 2016
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm HR_lab1_mod -w -dir netgen/synthesis -ofmt vhdl -sim HR_lab1_mod.ngc HR_lab1_mod_synthesis.vhd 
-- Device	: xc4vlx15-12-ff668
-- Input file	: HR_lab1_mod.ngc
-- Output file	: D:\lab1\HR_lab1\netgen\synthesis\HR_lab1_mod_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: HR_lab1_mod
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity HR_lab1_mod is
  port (
    hr_a : in STD_LOGIC := 'X'; 
    hr_b : in STD_LOGIC := 'X'; 
    hr_c : in STD_LOGIC := 'X'; 
    hr_d : in STD_LOGIC := 'X'; 
    hr_e : in STD_LOGIC := 'X'; 
    hr_res : out STD_LOGIC 
  );
end HR_lab1_mod;

architecture Structure of HR_lab1_mod is
  signal hr_a_IBUF_1 : STD_LOGIC; 
  signal hr_b_IBUF_3 : STD_LOGIC; 
  signal hr_c_IBUF_5 : STD_LOGIC; 
  signal hr_d_IBUF_7 : STD_LOGIC; 
  signal hr_e_IBUF_9 : STD_LOGIC; 
  signal hr_res1_11 : STD_LOGIC; 
  signal hr_res2_12 : STD_LOGIC; 
  signal hr_res_OBUF_13 : STD_LOGIC; 
begin
  hr_a_IBUF : IBUF
    port map (
      I => hr_a,
      O => hr_a_IBUF_1
    );
  hr_b_IBUF : IBUF
    port map (
      I => hr_b,
      O => hr_b_IBUF_3
    );
  hr_c_IBUF : IBUF
    port map (
      I => hr_c,
      O => hr_c_IBUF_5
    );
  hr_d_IBUF : IBUF
    port map (
      I => hr_d,
      O => hr_d_IBUF_7
    );
  hr_e_IBUF : IBUF
    port map (
      I => hr_e,
      O => hr_e_IBUF_9
    );
  hr_res_OBUF : OBUF
    port map (
      I => hr_res_OBUF_13,
      O => hr_res
    );
  hr_res1 : LUT4
    generic map(
      INIT => X"6AFF"
    )
    port map (
      I0 => hr_c_IBUF_5,
      I1 => hr_a_IBUF_1,
      I2 => hr_b_IBUF_3,
      I3 => hr_d_IBUF_7,
      O => hr_res1_11
    );
  hr_res2 : LUT4
    generic map(
      INIT => X"FF6A"
    )
    port map (
      I0 => hr_c_IBUF_5,
      I1 => hr_b_IBUF_3,
      I2 => hr_a_IBUF_1,
      I3 => hr_d_IBUF_7,
      O => hr_res2_12
    );
  hr_res_f5 : MUXF5
    port map (
      I0 => hr_res2_12,
      I1 => hr_res1_11,
      S => hr_e_IBUF_9,
      O => hr_res_OBUF_13
    );

end Structure;

