--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : RH_lab1_sch.vhf
-- /___/   /\     Timestamp : 10/13/2016 10:40:32
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family virtex4 -flat -suppress -vhdl D:/lab1/HR_lab1/RH_lab1_sch.vhf -w D:/lab1/HR_lab1/RH_lab1_sch.sch
--Design Name: RH_lab1_sch
--Device: virtex4
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity RH_lab1_sch is
   port ( hr_a   : in    std_logic; 
          hr_b   : in    std_logic; 
          hr_c   : in    std_logic; 
          hr_d   : in    std_logic; 
          hr_e   : in    std_logic; 
          hr_res : out   std_logic);
end RH_lab1_sch;

architecture BEHAVIORAL of RH_lab1_sch is
   attribute BOX_TYPE   : string ;
   signal XLXN_4 : std_logic;
   signal XLXN_6 : std_logic;
   signal XLXN_7 : std_logic;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>hr_b,
                I1=>hr_a,
                O=>XLXN_4);
   
   XLXI_2 : XOR2
      port map (I0=>hr_c,
                I1=>XLXN_4,
                O=>XLXN_6);
   
   XLXI_3 : XOR2
      port map (I0=>hr_e,
                I1=>hr_d,
                O=>XLXN_7);
   
   XLXI_4 : OR2
      port map (I0=>XLXN_7,
                I1=>XLXN_6,
                O=>hr_res);
   
end BEHAVIORAL;


