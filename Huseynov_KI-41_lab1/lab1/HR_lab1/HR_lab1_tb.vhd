-- Vhdl test bench created from schematic D:\lab1\HR_lab1\RH_lab1_sch.sch - Thu Oct 13 10:48:39 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY RH_lab1_sch_RH_lab1_sch_sch_tb IS
END RH_lab1_sch_RH_lab1_sch_sch_tb;
ARCHITECTURE behavioral OF RH_lab1_sch_RH_lab1_sch_sch_tb IS 

   COMPONENT RH_lab1_sch
   PORT( hr_a	:	IN	STD_LOGIC; 
          hr_b	:	IN	STD_LOGIC; 
          hr_d	:	IN	STD_LOGIC; 
          hr_c	:	IN	STD_LOGIC; 
          hr_res	:	OUT	STD_LOGIC; 
          hr_e	:	IN	STD_LOGIC);
   END COMPONENT;

   SIGNAL hr_a	:	STD_LOGIC:='0';
   SIGNAL hr_b	:	STD_LOGIC:='0';
   SIGNAL hr_d	:	STD_LOGIC:='0';
   SIGNAL hr_c	:	STD_LOGIC:='0';
   SIGNAL hr_res	:	STD_LOGIC:='0';
   SIGNAL hr_e	:	STD_LOGIC:='0';

BEGIN

   UUT: RH_lab1_sch PORT MAP(
		hr_a => hr_a, 
		hr_b => hr_b, 
		hr_d => hr_d, 
		hr_c => hr_c, 
		hr_res => hr_res, 
		hr_e => hr_e
   );

hr_a<=not hr_a after 10 ns;
hr_b<=not hr_b after 20 ns;
hr_c<=not hr_c after 40 ns;
hr_d<=not hr_d after 80 ns;
hr_e<=not hr_e after 160 ns;

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
