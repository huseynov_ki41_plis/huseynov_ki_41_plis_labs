<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="hr_a" />
        <signal name="hr_b" />
        <signal name="hr_d" />
        <signal name="XLXN_4" />
        <signal name="hr_c" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="hr_res" />
        <signal name="hr_e" />
        <port polarity="Input" name="hr_a" />
        <port polarity="Input" name="hr_b" />
        <port polarity="Input" name="hr_d" />
        <port polarity="Input" name="hr_c" />
        <port polarity="Output" name="hr_res" />
        <port polarity="Input" name="hr_e" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="hr_b" name="I0" />
            <blockpin signalname="hr_a" name="I1" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_2">
            <blockpin signalname="hr_c" name="I0" />
            <blockpin signalname="XLXN_4" name="I1" />
            <blockpin signalname="XLXN_6" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_3">
            <blockpin signalname="hr_e" name="I0" />
            <blockpin signalname="hr_d" name="I1" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_4">
            <blockpin signalname="XLXN_7" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="hr_res" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="768" y="752" name="XLXI_1" orien="R0" />
        <instance x="896" y="1248" name="XLXI_3" orien="R0" />
        <branch name="hr_a">
            <wire x2="768" y1="624" y2="624" x1="576" />
        </branch>
        <branch name="hr_b">
            <wire x2="768" y1="688" y2="688" x1="576" />
        </branch>
        <branch name="hr_d">
            <wire x2="896" y1="1120" y2="1120" x1="576" />
        </branch>
        <instance x="1280" y="960" name="XLXI_2" orien="R0" />
        <instance x="1840" y="960" name="XLXI_4" orien="R0" />
        <branch name="XLXN_4">
            <wire x2="1152" y1="656" y2="656" x1="1024" />
            <wire x2="1152" y1="656" y2="832" x1="1152" />
            <wire x2="1280" y1="832" y2="832" x1="1152" />
        </branch>
        <branch name="hr_c">
            <wire x2="1280" y1="896" y2="896" x1="576" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="1680" y1="864" y2="864" x1="1536" />
            <wire x2="1680" y1="832" y2="864" x1="1680" />
            <wire x2="1840" y1="832" y2="832" x1="1680" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="1552" y1="1152" y2="1152" x1="1152" />
            <wire x2="1552" y1="896" y2="1152" x1="1552" />
            <wire x2="1840" y1="896" y2="896" x1="1552" />
        </branch>
        <branch name="hr_res">
            <wire x2="2176" y1="864" y2="864" x1="2096" />
        </branch>
        <branch name="hr_e">
            <wire x2="896" y1="1184" y2="1184" x1="576" />
        </branch>
        <iomarker fontsize="28" x="576" y="624" name="hr_a" orien="R180" />
        <iomarker fontsize="28" x="576" y="688" name="hr_b" orien="R180" />
        <iomarker fontsize="28" x="576" y="896" name="hr_c" orien="R180" />
        <iomarker fontsize="28" x="576" y="1120" name="hr_d" orien="R180" />
        <iomarker fontsize="28" x="576" y="1184" name="hr_e" orien="R180" />
        <iomarker fontsize="28" x="2176" y="864" name="hr_res" orien="R0" />
    </sheet>
</drawing>