----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:58:17 10/13/2016 
-- Design Name: 
-- Module Name:    HR_lab1_mod - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity HR_lab1_mod is
    Port ( hr_a : in  STD_LOGIC;
           hr_b : in  STD_LOGIC;
           hr_c : in  STD_LOGIC;
           hr_d : in  STD_LOGIC;
           hr_e : in  STD_LOGIC;
           hr_res : out  STD_LOGIC);
end HR_lab1_mod;

architecture Behavioral of HR_lab1_mod is

begin
hr_res<=((hr_a and hr_b) xor hr_c) or (hr_d xor hr_e);

end Behavioral;

